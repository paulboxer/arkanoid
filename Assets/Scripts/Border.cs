﻿using UnityEngine;
using System.Collections;

public class Border : MonoBehaviour {
	    static Plane plane;
        void Init()
        {
            plane = new Plane(transform.forward, transform.position);  
            Vector3 leftHeight = CalcPosition(new Vector2(0f, 0f));
            Vector3 rightBootom = CalcPosition(new Vector2(Screen.width, Screen.height));
            Left = leftHeight.x;
            Right = rightBootom.x;
            Height = rightBootom.y;
            Bottom = leftHeight.y;
        }

        public float Left { get; private set; }
        public float Right { get; private set; }
        public float Height { get; private set; }
        public float Bottom { get; private set; }

        Vector3 CalcPosition(Vector2 screenPos)
        {
            //Ray ray = UICamera.currentCamera.ScreenPointToRay(screenPos);    // для NGUI
            Ray ray = Camera.main.ScreenPointToRay(screenPos);
            float dist = 0f;
            Vector3 pos = Vector3.zero;

            if (plane.Raycast(ray, out dist))
                pos = ray.GetPoint(dist);

            return pos;
        }      
    
    
	void Awake () {
        Init();
	}
	
}
