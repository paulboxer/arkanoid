﻿using UnityEngine;
using System.Collections;

public class Circle : Platform {

    Rigidbody2D _rigidbody2D;
    float StartGravityScale;
    public Platform platform;
    Vector2 currentVelocity;
    public float force = 500f;
    float radius = 1f;

    bool stop = true;
	void Start () {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        StartGravityScale = _rigidbody2D.gravityScale;
        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.rotation = 10;
        _rigidbody2D.gravityScale = 0;
	}

    void Update () {

        if (transform.position.x >= border.Right-radius)
        {
            transform.position = new Vector2(transform.position.x - 1, transform.position.y);
            _rigidbody2D.velocity = new Vector2(-_rigidbody2D.velocity.x, _rigidbody2D.velocity.y);            
        }

        if (transform.position.x <= border.Left + radius)
        {
            transform.position = new Vector2(transform.position.x + 1, transform.position.y);
            _rigidbody2D.velocity = new Vector2(-_rigidbody2D.velocity.x, _rigidbody2D.velocity.y);
        }
        
        if (transform.position.y <= border.Bottom)
        {            
            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.gravityScale = 0;
            stop = true;
        }
        
        if (transform.position.y >= border.Height-radius)
        {             
            transform.position = new Vector2(transform.position.x, transform.position.y-1);            
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, -_rigidbody2D.velocity.y);

        }
        
        if (stop && Input.GetKey(KeyCode.Space))
        {
            stop = false;
            _rigidbody2D.gravityScale = StartGravityScale;  
            if (Input.GetKey(KeyCode.LeftArrow)) _rigidbody2D.velocity = new Vector2(0 - 10, 0);
            else if (Input.GetKey(KeyCode.RightArrow)) _rigidbody2D.velocity = new Vector2(0 + 10, 0);
            else _rigidbody2D.velocity = new Vector2(0, 0);
            _rigidbody2D.AddForce(new Vector2(0, force));
        }

        if (stop)
        {
            Vector3 position = platform.transform.position;
            position.y = position.y + 1f;
            transform.position = position;
        }        
        if (_rigidbody2D.velocity != Vector2.zero) currentVelocity = _rigidbody2D.velocity;
	}

    void OnCollisionEnter2D(Collision2D myCollision)
    {        
        if (myCollision.gameObject.name == "Platform") 
        {
            _rigidbody2D.velocity = -currentVelocity;            
            if (Input.GetKey(KeyCode.LeftArrow))  _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x - 10, _rigidbody2D.velocity.y);
            else if (Input.GetKey(KeyCode.RightArrow)) _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x + 10, _rigidbody2D.velocity.y);
            else _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x - 10, _rigidbody2D.velocity.y);
        }        
    }
}
