﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

    public float deltaMove = 0.2f;
    public Border border;  
		
	void Update () {
        if (Input.GetKey(KeyCode.LeftArrow) && transform.position.x > border.Left)
        {                        
            transform.Translate(-deltaMove, 0, 0);            
        }
        if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < border.Right)
        {            
            transform.Translate(deltaMove, 0, 0);
        }        
	}
}
